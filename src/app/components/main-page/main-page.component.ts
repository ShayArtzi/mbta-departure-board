import { Component, OnInit } from '@angular/core';
import {MbtaApiService} from '../../services/mbta-api.service';
import {MbtaPrediction} from '../../services/preds/mbta-prediction';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss']
})
export class MainPageComponent implements OnInit {

  constructor(private mbtaApiService: MbtaApiService) { }

  preds: MbtaPrediction[];
  error = false;
  lastUpdateTime: Date;

  ngOnInit() {
    this.fetchData();
  }

  fetchData() {
    this.error = false;
    this.mbtaApiService.getPredictions().subscribe(res => {
      this.preds = res.data.filter( e => e.attributes.departure_time && this.isCommuter(e) );
      this.getTripsForPreds();
      this.lastUpdateTime = new Date();
    }, err => {
      this.error = true;
    });
  }

  getTripsForPreds() {
    this.preds.forEach( p => {
      const tripId = (p as any).relationships.trip.data.id;
      this.mbtaApiService.getTrip(tripId).subscribe(res => {
        p.trip = res.data;
      }, err => {
        this.error = true;
      });
    } );
  }

  extractTrackId(fullId: string) {
    const parts = fullId.split('-');
    return parts.length === 1 ? 'TBD' : parts[1];
  }

  private isCommuter(pred: MbtaPrediction) {
    const routeId = (pred as any).relationships.route.data.id as string;
    return routeId.indexOf('Green-') === -1 && routeId.indexOf('Orange') === -1;
  }

}

import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {MbtaResWrapper} from './mbta-res-wrapper';
import {MbtaPrediction} from './preds/mbta-prediction';
import {MbtaResArrayWrapper} from './mbta-res-array-wrapper';
import {MbtaTrip} from './trip/mbta-trip';

@Injectable({
  providedIn: 'root'
})
export class MbtaApiService {

  private urlPrefix = 'https://api-v3.mbta.com';

  constructor(private http: HttpClient) { }

  private get<T>(url: string): Observable<T> {
    const headers = new HttpHeaders({
      'X-API-Key': 'b54cfac1f4274a1e828f833a93870fef'
    });
    return this.http.get<T>(`${this.urlPrefix}/${url}`, {headers});
  }

  getPredictions(): Observable<MbtaResArrayWrapper<MbtaPrediction>> {
// return this.http.get<MbtaResArrayWrapper<MbtaPrediction>>(`${this.urlPrefix}/predictions?filter[stop]=place-north&sort=departure_time`);
    return this.get(`predictions?filter[stop]=place-north&sort=departure_time`);
  }

  getTrip(tripId: string): Observable<MbtaResWrapper<MbtaTrip>> {
    // return this.http.get<MbtaResWrapper<MbtaTrip>>(`${this.urlPrefix}/trips/${tripId}`);
    return this.get(`trips/${tripId}`);
  }


}

import {MbtaTripAttrs} from './mbta-trip-attrs';

export interface MbtaTrip {
  attributes: MbtaTripAttrs;
}

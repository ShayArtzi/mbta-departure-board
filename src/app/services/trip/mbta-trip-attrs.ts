export interface MbtaTripAttrs {
  headsign: string;
  name: string;
}

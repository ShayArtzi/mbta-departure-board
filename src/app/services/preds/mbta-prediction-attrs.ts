export interface MbtaPredictionAttrs {
  arrival_time: string;
  departure_time: string;
  direction_id: number;
  status: string;
}

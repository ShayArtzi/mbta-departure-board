import {MbtaPredictionAttrs} from './mbta-prediction-attrs';
import {MbtaTrip} from '../trip/mbta-trip';

export interface MbtaPrediction {
  attributes: MbtaPredictionAttrs;
  trip?: MbtaTrip;
}
